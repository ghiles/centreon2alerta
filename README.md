# STREAM CONNECTOR CENTREON2ALERTA
Script lua, permettant d'envoyer les alertes Centreon vers Alerta.

## Exemple d'alerte
```
{
    "origin": "PF/Central",
    "resource": "hostname",
    "event": "D Drive Usage",
    "group": "centreon",
    "severity": "warning",
    "environment": "Production",
    "service": [
        "Platform"
    ],
    "tags": [
        "check=Active"
    ],
    "text":"d:\ - total: 150.00 Gb - used: 122.97 Gb (82%) - free 27.02 Gb (18%)",
    "value": "1/3 (Soft)",
    "type": "centreonServiceAlert",
    "createTime": "2018-01-27T21:00:12.999Z",
}
```
